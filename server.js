const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.listen(port, function () {
  console.log('port: ' + port);
});

app.post('/approved', (req, res) => res.json({ message: 'Approved' }));

