import actionTypes from '../constants/actionTypes';

export const sendDataFromInputs = payload => ({ type: actionTypes.SEND_DATA, payload });
export const changePage = () => ({ type: actionTypes.CHANGE_PAGE });