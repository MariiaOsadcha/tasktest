import { combineReducers } from 'redux';
import { formReducer } from './reducers/formReducer';
import { mainReducer } from './reducers/mainReducer';

export default combineReducers({
    form: formReducer,
    main: mainReducer,
});