import actionTypes from '../../constants/actionTypes.js';

export const initialState = {
  dataForm: null,
};

export const formReducer = (state = initialState, action) => {
  switch (action.type) {
      case actionTypes.SET_DATA:
          return {
              ...state,
              dataForm: action.payload,
          };
      default:
          return state;
  }
};