import actionTypes from '../../constants/actionTypes.js';

export const initialState = {
    formPage: true,
};

export const mainReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_PAGE:
            return {
                ...state,
                formPage: !state.formPage,
            };
        default:
            return state;
    }
};