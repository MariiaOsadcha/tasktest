import React from 'react';
import Form from './components/form/Form.jsx';

function App() {
    return (
       <div className='wrapper'>
           <div className='form'>
               <Form/>
           </div>
       </div>
    );
}

export default App;