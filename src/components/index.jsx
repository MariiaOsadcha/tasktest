import { connect } from 'react-redux';
import Component from './Layout.jsx';
import * as actions from '../actions/actions';
import * as selector from '../selector/selector';

const mapStateToProps = (state) => ({
    dataForm: selector.getDataForm(state),
    formPage: selector.getStateFormPage(state),
});

const mapDispatchToProps = dispatch => ({
    sendRequest: payload => dispatch(actions.sendDataFromInputs(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);