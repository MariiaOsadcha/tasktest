import React from 'react';
import PropTypes from 'prop-types';
import './result.less';

const Result = props => {
   const { dataForm } = props;

    return (
        <div className='wrapper'>
            <span>{dataForm}</span>
        </div>
    );
};

Result.propTypes = {
    dataForm: PropTypes.string,
};

export default React.memo(Result);

