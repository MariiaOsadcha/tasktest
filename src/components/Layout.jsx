import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Form from './form/index.jsx';
import Result from './result/Result.jsx';

export default class Layout extends Component {
    render() {
        const { sendRequest, dataForm, formPage } = this.props;

        return (
            formPage ? <Form sendRequest={sendRequest}/> : <Result dataForm={dataForm}/>

        );
    }
}

Layout.propTypes = {
    sendRequest: PropTypes.func,
    dataForm: PropTypes.string,
    formPage: PropTypes.bool,

};

