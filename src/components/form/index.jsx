import { connect } from 'react-redux';
import Component from './Form.jsx';
import * as actions from '../../actions/actions';

const mapDispatchToProps = dispatch => ({
    sendRequest: payload => dispatch(actions.sendDataFromInputs(payload)),
});

export default connect(null, mapDispatchToProps)(Component);