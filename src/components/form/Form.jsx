import React from 'react';
import PropTypes from 'prop-types';
import './form.less';

export default class Form extends React.Component{

    inputRef = React.createRef();

     sendData = () => {
     const { sendRequest } = this.props;
         sendRequest({
             data: this.inputRef.current.value,
         });
    };

    render() {
        return (
            <div className='wrapper'>
                <input
                    ref={this.inputRef}
                    className= 'input'
                    type='text' id='id'/>
                 <button
                    id='button'
                    className='button'
                    onClick= { this.sendData }
                 />
            </div>
        );
    }
}

Form.propTypes = {
  sendRequest: PropTypes.func,
    history: PropTypes.object,
};