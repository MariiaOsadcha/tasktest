import keyMirror from 'key-mirror';

export default keyMirror({
   SEND_DATA: null,
   SET_DATA: null,
   CHANGE_PAGE: null,
});