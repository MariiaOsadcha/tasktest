import React from 'react';
import ReactDOM from 'react-dom';
import Layout from './components/index.jsx';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';

import rootReducer from './reducer/rootReducer';
import rootSaga from './saga/rootSaga.js';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
window.store = store;
sagaMiddleware.run(rootSaga);

ReactDOM.render(
    <Provider store={store}>
        <Layout />
    </Provider>,
    document.getElementById('root')
);
