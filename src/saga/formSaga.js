import { put, apply, takeLatest } from '@redux-saga/core/effects';
import actionTypes from '../constants/actionTypes.js';
import axios from 'axios';

export function* watchSendData() {
    yield takeLatest(actionTypes.SEND_DATA, sendData);
}

export function* sendData(action) {
    const { data } = action.payload;
    try {
        const url = 'http://localhost:3000/approved';
        const headers = {
            'Content-Type': 'application/json; charset=UTF-8',
        };
        const response = yield apply(axios, axios.post, [url, data, headers]);
            yield put({ type: actionTypes.SET_DATA, payload: response.data.message });
            yield put({ type: actionTypes.CHANGE_PAGE });
    } catch (err) {
        console.error(err);
    }
}