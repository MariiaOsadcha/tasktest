import { all, fork } from 'redux-saga/effects';
import { watchSendData } from './formSaga.js';

const sagas = [
    watchSendData,
];

export default function* rootSaga() {
    yield all(sagas.map(fork));
}